#!/bin/bash
#
# Author: Sergio Ribera
# Repository: https://github.com/SergioRibera/AFORO22-DevOps-Final
# License: GPL v3
#

NC='\033[0m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
YELLOW='\e[0;33m\033[1m'


WORKDIR=$(pwd)
PROYECTS=(
    "Gateway"
    "Invoice"
    "Pay"
    "Security"
    "Transaction"
)

trap ctrl_c INT

function clean_docker() {
    printf "[ $BLUE-$NC ] ${BLUE}Clean intermediate images ${GREEN}$2$NC\n"
    docker image prune --filter "label=stage=builder"
}

function ctrl_c(){
    echo -e "\n[ ${YELLOW}*$NC ] Saliendo...\n"
    clean_docker
    exit 0
}

build_image() {
    cd $1
    printf "[ $BLUE-$NC ] ${BLUE}Removing latest image ${GREEN}$2$NC\n"
    docker rmi "$2:latest" &>/dev/null
    printf "[ $BLUE-$NC ] ${BLUE}Building image ${GREEN}$2$NC\n"
    docker build -t "$2:latest" . &>>build.log
    if [ $? -eq 0 ]; then
        printf "[ $GREEN+$NC ] ${GREEN}Image $2 built successfully$NC\n"
    else
        printf "[ $RED-$NC ] ${RED}Error building image, see build.log $2$NC\n"
    fi
}

for i in ${!PROYECTS[@]}; do
    IMG_NAME="app-${PROYECTS[$i],,}-img"
    build_image "$WORKDIR/AFORO255.MS.TEST.${PROYECTS[$i]}" $IMG_NAME
done

printf "[ $GREEN*$NC ] ${GREEN}Done$NC\n"
