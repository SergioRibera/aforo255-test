﻿using MS.AFORO255.Invoice.Models;

namespace MS.AFORO255.Invoice.Services
{
    public interface ITransactionService
    {
        Transaction Invoice(Transaction transaction);
        Transaction InvoiceReverse(Transaction transaction);
    }
}
