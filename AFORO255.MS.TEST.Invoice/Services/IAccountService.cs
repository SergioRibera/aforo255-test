﻿using MS.AFORO255.Invoice.DTOs;
using MS.AFORO255.Invoice.Models;
using System.Threading.Tasks;

namespace MS.AFORO255.Invoice.Services
{
    public interface IAccountService
    {
        Task<bool> InvoiceAccount(AccountRequest request);
        bool InvoiceReverse(Transaction request);
        bool Execute(Transaction request);
    }
}
