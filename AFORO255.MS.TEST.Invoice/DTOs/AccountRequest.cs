﻿namespace MS.AFORO255.Invoice.DTOs
{
    public class AccountRequest
    {
        public int IdAccount { get; set; }
        public decimal Amount { get; set; }

    }
}
