﻿namespace MS.AFORO255.Invoice.DTOs
{
    public class TransactionRequest
    {
        public int AccountId { get; set; }
        public decimal Amount { get; set; }

    }
}
