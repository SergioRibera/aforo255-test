﻿using MS.AFORO255.Invoice.Repositories;

namespace MS.AFORO255.Invoice.Data
{
    public class DbInitializer
    {
        public static void Initialize(ContextDatabase context)
        {
            context.Database.EnsureCreated();
            context.SaveChanges();
        }
    }
}
