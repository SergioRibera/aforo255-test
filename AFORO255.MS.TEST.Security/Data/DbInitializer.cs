﻿using MS.AFORO255.Security.Repositories;
using System.Linq;

namespace MS.AFORO255.Security.Data
{
    public class DbInitializer
    {
        public static void Initialize(ContextDatabase context)
        {
            context.Database.EnsureCreated();

            if (context.Access.Any())
            {
                return;   
            }
            var orders = new Models.Access[]
            {
                new Models.Access{Username="sribera",Password="123456"},
                new Models.Access{Username="cribera",Password="654321"},
                new Models.Access{Username="cr7",Password="123"},
            };
            foreach (Models.Access s in orders)
            {
                context.Access.Add(s);
            }
            context.SaveChanges();
        }
    }
}
